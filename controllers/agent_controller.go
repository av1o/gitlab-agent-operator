/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"time"

	gitlabv1alpha1 "gitlab.com/av1o/gitlab-agent-operator/api/v1alpha1"
	agentctl "gitlab.com/av1o/gitlab-agent-operator/controllers/agent"
	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
)

var (
	// ErrTokenEmpty returns when token secret is valid
	// but the 'token' key is empty
	ErrTokenEmpty = fmt.Errorf("token cannot be empty")
)

// AgentReconciler reconciles a Agent object
type AgentReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=apps.gitlab.com,resources=agents,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps.gitlab.com,resources=agents/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=apps.gitlab.com,resources=agents/finalizers,verbs=update
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=serviceaccounts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch

// Reconcile triggers when an event occurs on the watched resource
func (r *AgentReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx).WithValues("agent", req.NamespacedName)

	logger.Info("Reconciling Agent", "name", req.NamespacedName.Name, "namespace", req.NamespacedName.Namespace)
	agent := &gitlabv1alpha1.Agent{}
	if err := r.Get(ctx, req.NamespacedName, agent); err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}
	if agent.DeletionTimestamp != nil {
		return ctrl.Result{}, nil
	}

	if err := r.validateTokenSecret(ctx, agent); err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{RequeueAfter: time.Second * 10}, nil
		}
		return ctrl.Result{}, err
	}

	if err := r.reconcileServiceAccount(ctx, agent); err != nil {
		return ctrl.Result{}, err
	}

	if err := r.reconcileDeployments(ctx, agent); err != nil {
		return ctrl.Result{}, err
	}

	if err := r.reconcileService(ctx, agent); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *AgentReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&gitlabv1alpha1.Agent{}).
		Owns(&corev1.Service{}).
		Owns(&appsv1.Deployment{}).
		Complete(r)
}

func (r *AgentReconciler) reconcileService(ctx context.Context, cr *gitlabv1alpha1.Agent) error {
	svc := agentctl.Service(cr)

	found := &corev1.Service{}
	if err := r.Get(ctx, types.NamespacedName{Name: svc.Name, Namespace: cr.Namespace}, found); err != nil {
		if errors.IsNotFound(err) {
			if err := ctrl.SetControllerReference(cr, svc, r.Scheme); err != nil {
				return err
			}
			if err := r.Create(ctx, svc); err != nil {
				setStatusCreateErr(cr, ConditionService, err)
				return err
			}
			return nil
		}
		setStatusErr(cr, ConditionService, err)
		return err
	}
	setStatusReady(cr, ConditionService)
	// disabled to match the behaviour
	// of the gitlab-runner-operator

	//if !reflect.DeepEqual(svc.Spec, found.Spec) {
	//	return r.Update(ctx, found)
	//}
	return nil
}

func (r *AgentReconciler) reconcileDeployments(ctx context.Context, cr *gitlabv1alpha1.Agent) error {
	logger := log.FromContext(ctx)
	agent, err := agentctl.Deployment(cr)
	if err != nil {
		logger.Error(err, "failed to generate Deployment from Agent")
		return err
	}

	found := &appsv1.Deployment{}
	if err := r.Get(ctx, types.NamespacedName{Name: agent.Name, Namespace: cr.Namespace}, found); err != nil {
		if errors.IsNotFound(err) {
			if err := ctrl.SetControllerReference(cr, agent, r.Scheme); err != nil {
				return err
			}
			if err := r.Create(ctx, agent); err != nil {
				setStatusCreateErr(cr, ConditionDeployment, err)
				return err
			}
			return nil
		}
		setStatusErr(cr, ConditionDeployment, err)
		return err
	}
	// reconcile
	deployment, changed := gitlabutils.IsDeploymentChanged(found, agent)
	if changed {
		return r.Update(ctx, deployment)
	}
	setStatusReady(cr, ConditionDeployment)
	return nil
}

func (r *AgentReconciler) validateTokenSecret(ctx context.Context, cr *gitlabv1alpha1.Agent) error {
	logger := log.FromContext(ctx, "agent", cr)

	// check if the token secret exists
	tokenSecret := &corev1.Secret{}
	tokenKey := types.NamespacedName{
		Name:      cr.Spec.TokenSecretRef.Name,
		Namespace: cr.Namespace,
	}
	// fallback to the deprecated field
	if tokenKey.Name == "" {
		tokenKey.Name = cr.Spec.Token
	}
	if err := r.Get(ctx, tokenKey, tokenSecret); err != nil {
		if errors.IsNotFound(err) {
			logger.Error(err, "token secret not found", "name", tokenKey.Name)
			meta.SetStatusCondition(&cr.Status.Conditions, metav1.Condition{
				Type:    ConditionSecretToken,
				Status:  metav1.ConditionFalse,
				Reason:  ReasonNotFound,
				Message: err.Error(),
			})
			return err
		}
		setStatusErr(cr, ConditionSecretToken, err)
		return err
	}
	tokenSecretKey := cr.Spec.TokenSecretRef.Key
	if tokenSecretKey == "" {
		tokenSecretKey = "token"
	}
	token, ok := tokenSecret.Data[tokenSecretKey]
	if !ok {
		err := fmt.Errorf("token key '%s' not found in secret '%s'", tokenSecretKey, tokenSecret.Name)
		setStatusErr(cr, ConditionSecretToken, err)
		return err
	}
	tokenStr := string(token)
	if tokenStr == "" {
		setStatusErr(cr, ConditionSecretToken, ErrTokenEmpty)
		return ErrTokenEmpty
	}
	setStatusReady(cr, ConditionSecretToken)
	return nil
}

// reconcileServiceAccount checks if the gitlab agent service account exists.
// If not, it creates it
func (r *AgentReconciler) reconcileServiceAccount(ctx context.Context, cr *gitlabv1alpha1.Agent) error {
	sa := &corev1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name,
			Namespace: cr.Namespace,
			Labels: map[string]string{
				"app.kubernetes.io/name":       cr.Name,
				"app.kubernetes.io/created-by": "gitlab-agent-operator",
			},
		},
	}
	lookupKey := types.NamespacedName{
		Name:      sa.Name,
		Namespace: cr.Namespace,
	}
	found := &corev1.ServiceAccount{}
	if err := r.Get(ctx, lookupKey, found); err != nil {
		if errors.IsNotFound(err) {
			if err := ctrl.SetControllerReference(cr, sa, r.Scheme); err != nil {
				return err
			}
			if err := r.Create(ctx, sa); err != nil {
				setStatusCreateErr(cr, ConditionServiceAccount, err)
				return err
			}
			return nil
		}
		setStatusErr(cr, ConditionServiceAccount, err)
		return err
	}
	setStatusReady(cr, ConditionServiceAccount)
	return nil
}
