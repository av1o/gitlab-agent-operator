package agent

import (
	"github.com/stretchr/testify/assert"
	gitlabv1alpha1 "gitlab.com/av1o/gitlab-agent-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"testing"
)

func TestDeployment_Token(t *testing.T) {
	t.Run("new secret is provided", func(t *testing.T) {
		dep, err := Deployment(&gitlabv1alpha1.Agent{
			Spec: gitlabv1alpha1.AgentSpec{
				TokenSecretRef: corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: "my-secret",
					},
					Key: "secret-value",
				},
			},
		})
		assert.NoError(t, err)
		assert.EqualValues(t, "my-secret", dep.Spec.Template.Spec.Volumes[0].VolumeSource.Secret.SecretName)
	})
	t.Run("legacy secret is provided", func(t *testing.T) {
		dep, err := Deployment(&gitlabv1alpha1.Agent{
			Spec: gitlabv1alpha1.AgentSpec{
				Token: "foobar",
				TokenSecretRef: corev1.SecretKeySelector{
					Key: "secret-value",
				},
			},
		})
		assert.NoError(t, err)
		assert.EqualValues(t, "foobar", dep.Spec.Template.Spec.Volumes[0].VolumeSource.Secret.SecretName)
	})
	t.Run("new secret is preferred", func(t *testing.T) {
		dep, err := Deployment(&gitlabv1alpha1.Agent{
			Spec: gitlabv1alpha1.AgentSpec{
				Token: "foobar",
				TokenSecretRef: corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: "my-secret",
					},
					Key: "secret-value",
				},
			},
		})
		assert.NoError(t, err)
		assert.EqualValues(t, "my-secret", dep.Spec.Template.Spec.Volumes[0].VolumeSource.Secret.SecretName)
	})
}

func TestDeployment_CA(t *testing.T) {
	t.Run("new secret is provided", func(t *testing.T) {
		dep, err := Deployment(&gitlabv1alpha1.Agent{
			Spec: gitlabv1alpha1.AgentSpec{
				Token: "test",
				CASecretRef: corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: "my-secret",
					},
					Key: "my-ca.crt",
				},
			},
		})
		assert.NoError(t, err)
		assert.EqualValues(t, "my-secret", dep.Spec.Template.Spec.Volumes[1].VolumeSource.Secret.SecretName)
	})
	t.Run("legacy secret is provided", func(t *testing.T) {
		dep, err := Deployment(&gitlabv1alpha1.Agent{
			Spec: gitlabv1alpha1.AgentSpec{
				Token:                "test",
				CertificateAuthority: "foobar",
				CASecretRef: corev1.SecretKeySelector{
					Key: "secret-value",
				},
			},
		})
		assert.NoError(t, err)
		assert.EqualValues(t, "foobar", dep.Spec.Template.Spec.Volumes[1].VolumeSource.Secret.SecretName)
	})
	t.Run("new secret is preferred", func(t *testing.T) {
		dep, err := Deployment(&gitlabv1alpha1.Agent{
			Spec: gitlabv1alpha1.AgentSpec{
				Token:                "test",
				CertificateAuthority: "foobar",
				CASecretRef: corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: "my-secret",
					},
					Key: "secret-value",
				},
			},
		})
		assert.NoError(t, err)
		assert.EqualValues(t, "my-secret", dep.Spec.Template.Spec.Volumes[1].VolumeSource.Secret.SecretName)
	})
}
