package agent

import (
	gitlabv1alpha1 "gitlab.com/av1o/gitlab-agent-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func Service(cr *gitlabv1alpha1.Agent) *corev1.Service {
	labels := Labels(cr)
	return &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name,
			Namespace: cr.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			Selector: labels,
			Ports: []corev1.ServicePort{
				{
					Name:     "metrics",
					Protocol: corev1.ProtocolTCP,
					Port:     8080,
				},
			},
			Type: corev1.ServiceTypeClusterIP,
		},
	}
}
