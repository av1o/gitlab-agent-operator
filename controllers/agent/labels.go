package agent

import gitlabv1alpha1 "gitlab.com/av1o/gitlab-agent-operator/api/v1alpha1"

func Labels(cr *gitlabv1alpha1.Agent) map[string]string {
	return map[string]string{
		"app.kubernetes.io/name":       cr.Name,
		"app.kubernetes.io/instance":   cr.Name,
		"app.kubernetes.io/component":  componentName,
		"app.kubernetes.io/managed-by": "gitlab-agent-operator",
	}
}
