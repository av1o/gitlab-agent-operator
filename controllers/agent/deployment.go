package agent

import (
	"errors"
	"fmt"
	gitlabv1alpha1 "gitlab.com/av1o/gitlab-agent-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/utils/pointer"
	"os"
	"path/filepath"
)

const (
	volumeSecret     = "secret-volume"
	volumeConfig     = "config-volume"
	volumeKubeConfig = "kube-config-volume"

	mountSecret     = "/etc/agentk/secrets"
	mountConfig     = "/etc/agentk/config"
	mountKubeConfig = "/etc/agentk/.kube"

	componentName = "gitlab-agent"

	defaultAgentImage     = "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:v16.11.0"
	defaultTokenSecretKey = "token"
	defaultCASecretKey    = "ca.crt"

	envDefaultAgentImage = "DEFAULT_AGENT_IMAGE"
)

//goland:noinspection GoDeprecation
func Deployment(cr *gitlabv1alpha1.Agent) (*appsv1.Deployment, error) {
	labels := Labels(cr)
	agentImage := cr.Spec.AgentImage
	if agentImage == "" {
		agentImage = os.Getenv(envDefaultAgentImage)
		if agentImage == "" {
			agentImage = defaultAgentImage
		}
	}
	// default to the token for backwards compatability
	tokenSecretName := cr.Spec.TokenSecretRef.Name
	tokenSecretKey := cr.Spec.TokenSecretRef.Key
	// fallback to the deprecated field
	if tokenSecretName == "" {
		tokenSecretName = cr.Spec.Token
		tokenSecretKey = defaultTokenSecretKey
	}
	// ensure that a token has been provided
	if tokenSecretName == "" {
		return nil, errors.New(".spec.tokenSecretRef and deprecated field .spec.token are both nil")
	}
	if tokenSecretKey == "" {
		tokenSecretKey = defaultTokenSecretKey
	}
	pullPolicy := cr.Spec.ImagePullPolicy
	if pullPolicy == "" {
		pullPolicy = corev1.PullIfNotPresent
	}
	args := []string{
		fmt.Sprintf("--token-file=%s/%s", mountSecret, tokenSecretKey),
		fmt.Sprintf("--kas-address=%s", cr.Spec.KAS),
	}

	volumeMounts := []corev1.VolumeMount{
		{
			Name:      volumeSecret,
			MountPath: mountSecret,
			ReadOnly:  true,
		},
	}

	volumes := []corev1.Volume{
		{
			Name: volumeSecret,
			VolumeSource: corev1.VolumeSource{
				Secret: &corev1.SecretVolumeSource{
					SecretName: tokenSecretName,
				},
			},
		},
	}

	podNs := corev1.EnvVar{
		Name: "POD_NAMESPACE",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{
				FieldPath: "metadata.namespace",
			},
		},
	}

	// if the user has requested it, allow
	// them to specify a custom kubeconfig.
	hasKubeConfig := cr.Spec.KubeConfigRef.Name != ""
	if hasKubeConfig {
		args = append(args, fmt.Sprintf("--kubeconfig=%s", filepath.Join("/etc/agentk/.kube", cr.Spec.KubeConfigRef.Key)))
		volumes = append(volumes, corev1.Volume{
			Name: volumeKubeConfig,
			VolumeSource: corev1.VolumeSource{
				Secret: &corev1.SecretVolumeSource{
					SecretName: cr.Spec.KubeConfigRef.Name,
				},
			},
		})
		volumeMounts = append(volumeMounts, corev1.VolumeMount{
			Name:      volumeKubeConfig,
			MountPath: mountKubeConfig,
			ReadOnly:  true,
		})
		if cr.Spec.KubeConfigNamespace != "" {
			podNs.ValueFrom = nil
			podNs.Value = cr.Spec.KubeConfigNamespace
		}
	}

	hasCA := cr.Spec.CertificateAuthority != "" || cr.Spec.CASecretRef.Name != ""
	caSecretName := cr.Spec.CASecretRef.Name
	caSecretKey := cr.Spec.CASecretRef.Key
	if caSecretName == "" {
		caSecretName = cr.Spec.CertificateAuthority
		caSecretKey = defaultCASecretKey
	}
	if caSecretKey == "" {
		caSecretKey = defaultCASecretKey
	}
	if hasCA {
		args = append(args, fmt.Sprintf("--ca-cert-file=%s/%s", mountConfig, caSecretKey))
		volumes = append(volumes, corev1.Volume{
			Name: volumeConfig,
			VolumeSource: corev1.VolumeSource{
				Secret: &corev1.SecretVolumeSource{
					SecretName: caSecretName,
				},
			},
		})
		volumeMounts = append(volumeMounts, corev1.VolumeMount{
			Name:      volumeConfig,
			MountPath: mountConfig,
			ReadOnly:  true,
		})
	}

	agent := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name,
			Namespace: cr.Namespace,
			Labels:    labels,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: pointer.Int32(1),
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					ServiceAccountName: cr.Name,
					SecurityContext:    &cr.Spec.PodSecurityContext,
					Containers: []corev1.Container{
						{
							Name:            componentName,
							Image:           agentImage,
							ImagePullPolicy: pullPolicy,
							Args:            args,
							LivenessProbe: &corev1.Probe{
								InitialDelaySeconds: 15,
								PeriodSeconds:       20,
								ProbeHandler: corev1.ProbeHandler{
									HTTPGet: &corev1.HTTPGetAction{
										Path: "/liveness",
										Port: intstr.FromInt(8080),
									},
								},
							},
							ReadinessProbe: &corev1.Probe{
								InitialDelaySeconds: 5,
								PeriodSeconds:       10,
								ProbeHandler: corev1.ProbeHandler{
									HTTPGet: &corev1.HTTPGetAction{
										Path: "/readiness",
										Port: intstr.FromInt(8080),
									},
								},
							},
							SecurityContext: &cr.Spec.SecurityContext,
							Resources:       cr.Spec.Resources,
							Env: []corev1.EnvVar{
								podNs,
								{
									Name: "POD_NAME",
									ValueFrom: &corev1.EnvVarSource{
										FieldRef: &corev1.ObjectFieldSelector{
											FieldPath: "metadata.name",
										},
									},
								},
							},
							VolumeMounts: volumeMounts,
						},
					},
					Volumes: volumes,
				},
			},
		},
	}
	// set service account
	if cr.Spec.ServiceAccount != "" {
		agent.Spec.Template.Spec.ServiceAccountName = cr.Spec.ServiceAccount
	}
	return agent, nil
}
