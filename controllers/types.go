package controllers

const (
	ConditionSecretToken    = "SecretTokenReady"
	ConditionSecretConfig   = "SecretConfigReady"
	ConditionDeployment     = "DeploymentReady"
	ConditionService        = "ServiceReady"
	ConditionServiceAccount = "ServiceAccountReady"
)

const (
	ReasonCreated     = "Created"
	ReasonError       = "Error"
	ReasonErrorCreate = "ErrCreate"
	ReasonNotFound    = "ErrNotFound"
	ReasonReady       = "Ready"
)
