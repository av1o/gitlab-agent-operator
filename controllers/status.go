package controllers

import (
	gitlabv1alpha1 "gitlab.com/av1o/gitlab-agent-operator/api/v1alpha1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func setStatusReady(cr *gitlabv1alpha1.Agent, t string) {
	meta.SetStatusCondition(&cr.Status.Conditions, metav1.Condition{
		Type:   t,
		Status: metav1.ConditionTrue,
		Reason: ReasonReady,
	})
}

func setStatusErr(cr *gitlabv1alpha1.Agent, t string, err error) {
	meta.SetStatusCondition(&cr.Status.Conditions, metav1.Condition{
		Type:    t,
		Status:  metav1.ConditionFalse,
		Reason:  ReasonError,
		Message: err.Error(),
	})
}

func setStatusCreateErr(cr *gitlabv1alpha1.Agent, t string, err error) {
	meta.SetStatusCondition(&cr.Status.Conditions, metav1.Condition{
		Type:    t,
		Status:  metav1.ConditionFalse,
		Reason:  ReasonErrorCreate,
		Message: err.Error(),
	})
}
