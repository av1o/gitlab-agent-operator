/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// AgentSpec defines the desired state of Agent
type AgentSpec struct {
	// The fully qualified address of the GitLab KAS instance.
	// For example, wss://kas.gitlab.example.com
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="GitLab KAS URL",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	KAS string `json:"kas"`

	// Deprecated: prefer TokenSecretRef
	//
	// Name of the Secret containing the `token` key used to register the agent
	Token string `json:"token,omitempty"`

	// Reference to the Secret containing the `token` key used to register the agent
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Token Secret",xDescriptors="urn:alm:descriptor:io.kubernetes:Secret"
	TokenSecretRef corev1.SecretKeySelector `json:"tokenSecretRef,omitempty"`

	// If specified, overrides the default GitLab Agent image. Default is the Agent image the operator was bundled with.
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Agent Image",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text,urn:alm:descriptor:com.tectonic.ui:advanced"
	AgentImage string `json:"agentImage,omitempty"`

	// Deprecated: prefer CASecretRef
	//
	// Name of the TLS Secret containing the custom certificate
	// authority (CA) certificates.
	CertificateAuthority string `json:"ca,omitempty"`

	// Reference to the TLS Secret containing the custom certificate
	// authority (CA) certificate.
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Certificate Authority Secret",xDescriptors="urn:alm:descriptor:io.kubernetes:Secret,urn:alm:descriptor:com.tectonic.ui:advanced"
	CASecretRef corev1.SecretKeySelector `json:"caSecretRef,omitempty"`

	// Allow user to override service account
	// used by the GitLab Agent
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="ServiceAccount",xDescriptors="urn:alm:descriptor:io.kubernetes.ServiceAccount,urn:alm:descriptor:com.tectonic.ui:advanced"
	ServiceAccount string `json:"serviceAccount,omitempty"`

	// ImagePullPolicy controls when Kubernetes pulls the image.
	// One of Always, Never, IfNotPresent.
	// Defaults to Always if :latest tag is specified, or IfNotPresent otherwise.
	// More info: https://kubernetes.io/docs/concepts/containers/images#updating-images
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="ImagePullPolicy",xDescriptors="urn:alm:descriptor:com.tectonic.ui:imagePullPolicy,urn:alm:descriptor:com.tectonic.ui:advanced"
	ImagePullPolicy corev1.PullPolicy `json:"imagePullPolicy,omitempty"`

	// Allow user to override resources
	// used by the GitLab Agent
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Resources",xDescriptors="urn:alm:descriptor:com.tectonic.ui:resourceRequirements,urn:alm:descriptor:com.tectonic.ui:advanced"
	Resources corev1.ResourceRequirements `json:"resources,omitempty"`

	// Allow user to override container securityContext
	// used by the GitLab Agent
	SecurityContext corev1.SecurityContext `json:"securityContext,omitempty"`

	// Allow user to override pod securityContext
	// used by the GitLab Agent
	PodSecurityContext corev1.PodSecurityContext `json:"podSecurityContext,omitempty"`

	// Name of the Secret containing a Kubernetes Configuration YAML which
	// the Agent will use to connect to a different cluster than its own.
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="External Cluster Credentials",xDescriptors="urn:alm:descriptor:io.kubernetes:Secret,urn:alm:descriptor:com.tectonic.ui:advanced"
	KubeConfigRef corev1.SecretKeySelector `json:"kubeConfigRef,omitempty"`

	// Namespace to use for Agent configuration (e.g. leader election) when
	// connecting to a different cluster. Only used when KubeConfigRef
	// is set.
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="External Cluster Namespace",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text,urn:alm:descriptor:com.tectonic.ui:advanced"
	KubeConfigNamespace string `json:"kubeConfigNamespace,omitempty"`
}

// AgentStatus defines the observed state of Agent
type AgentStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
// +operator-sdk:csv:customresourcedefinitions:displayName="GitLab Agent"
// +operator-sdk:csv:customresourcedefinitions:resources={{Secret,v1,""},{Service,v1,""},{Pod,v1,""},{Deployment,v1,""}}

// Agent for Kubernetes is a way to integrate your cluster with GitLab in a secure way.
type Agent struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   AgentSpec   `json:"spec,omitempty"`
	Status AgentStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// AgentList contains a list of Agent
type AgentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Agent `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Agent{}, &AgentList{})
}
