[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# GitLab Agent Operator

The GitLab Agent operator aims to manage the lifecycle of GitLab Agent instances in your Kubernetes or Openshift container platforms.

The GitLab Agent operator aims to:

- ease installation and configuration of GitLab Agent instances
- offer seamless upgrades from version to version

## Requirements

The GitLab Agent operator uses native Kubernetes resources to deploy and manage GitLab Agent instances in your cluster. It therefore will presumably run on any container platform that is derived from Kubernetes.

## Owned Resources

The operator owns, watches and reconciles three different primary resources at this time.

#### 1. GitLab Agent

GitLab Agent for Kubernetes is a way to integrate your cluster with GitLab in a secure way. It is used in conjunction with GitLab CI/CD, the open-source continuous integration service included with GitLab that coordinates jobs.

An example is shown below:

```
apiVersion: apps.gitlab.com/v1alpha1
kind: Agent
metadata:
  name: example
spec:
  kas: wss://kas.gitlab.com
  token: agent-token-secret # Name of the secret containing the Agent token
```

## Developing

Make sure to read the [DEVELOPING.md](DEVELOPING.md) document to get started.
