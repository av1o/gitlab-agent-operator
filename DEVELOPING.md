# Developing

## Making changes

When making changes to the [`agent_types.go`](api/v1alpha1/agent_types.go) file, make sure to apply them.
Avoid introducing breaking changes.

```shell
# generate new types and manifests
make generate
make manifests

# make your code changes so that new/updated fields are reconciled correctly
```

## Testing changes 

TODO
